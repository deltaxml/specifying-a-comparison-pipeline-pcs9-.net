// $Id$
// Copyright (c) 2011 DeltaXML Ltd. All rights reserved

using DeltaXML.CoreS9Api;
using System;
using System.IO;
using System.Collections.Generic;
using com.deltaxml.pipe.filters;
using net.sf.saxon.s9api;

namespace DeltaXML.Demos {
  public class DemoPipeline {
  
    PipelinedComparatorS9 pc;

    bool runADefault= true;
    bool dontRunBDefault= false;
    bool tidyInputsDefault= true;
    bool inputAlreadyTidyDefault= false;
    
    string normalizeAttValuesDefault= "true";

    public void setUpPipeline() {
       setUpPipeline(runADefault, dontRunBDefault, tidyInputsDefault, inputAlreadyTidyDefault, normalizeAttValuesDefault);
    }   
  
    public void setUpPipeline(bool runA, bool dontRunB, bool tidyInputs, bool inputAlreadyTidy, string normalizeAttValues)  {
    
    Directory.SetCurrentDirectory(@"../..");
    pc= new PipelinedComparatorS9();
    
    IList<Object> input1Filters= new List<Object>();
    if (tidyInputs && !inputAlreadyTidy) {
      input1Filters.Add(new FileInfo("tidy-input.xsl"));
    }
    if (runA) {
      input1Filters.Add(new FileInfo("input-filterA.xsl"));
    }
    if (!dontRunB) {
      input1Filters.Add(new FileInfo("input-filterB.xsl"));
    }
    ParameterizedFilterS9 pf= new ParameterizedFilterS9(new FileInfo("input-filter-with-parameter.xsl"));
    pf["an-input-param"]="input1";
    input1Filters.Add(pf);
    
    IList<Object> input2Filters= new List<Object>();
    if (tidyInputs && !inputAlreadyTidy) {
      input2Filters.Add(new FileInfo("tidy-input.xsl"));
    }
    input2Filters.Add(new FileInfo("input-filterA.xsl"));
    pf= new ParameterizedFilterS9(new FileInfo("input-filter-with-parameter.xsl"));
    pf["an-input-param"] = "input2";
    input2Filters.Add(pf);
    
    IList<Object> outputFilters= new List<Object>();
    pf= new ParameterizedFilterS9(typeof(NormalizeSpace));
    pf["normalizeAttValues"] = normalizeAttValues;
    outputFilters.Add(pf);
    
    pc.setInput1Filters(input1Filters);
    pc.setInput2Filters(input2Filters);
    pc.setOutputFilters(outputFilters);
    
    pc.setOutputProperty(Serializer.Property.INDENT, "yes");
    pc.setParserFeature("http://apache.org/xml/features/xinclude", true);
    pc.setComparatorFeature("http://deltaxml.com/api/feature/isFullDelta", true);
    pc.setComparatorProperty("http://deltaxml.com/api/property/orderlessPresentation", "a_matches_deletes_adds");
  }
  
    public void runComparison(FileInfo input1, FileInfo input2, FileInfo result) {
      pc.compare(input1, input2, result);
    }

    public static void Main(string [] args) {
      Console.WriteLine("Running pipeline with default parameter settings...");
      DemoPipeline demo= new DemoPipeline();
      demo.setUpPipeline();
      FileInfo outfile = new FileInfo("result.xml");
      demo.runComparison(new FileInfo("demo-input.xml"), new FileInfo("demo-input.xml"), outfile);
      Console.WriteLine("Output file saved to: " + outfile.FullName);
      Console.WriteLine("Press any key to exit...");
      Console.ReadKey();
    }
  }
}
