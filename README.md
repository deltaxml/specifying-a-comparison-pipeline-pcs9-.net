# Specifying a Comparison Pipeline

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML Compare release. The resources should be located such that they are two levels below the top level release directory.*

*For example `DeltaXML-XML-Compare-10_0_0_n/samples/specifying-a-comparison-pipeline`.*

---

The resources here, demonstrate how to set up a PipelinedComparatorS9. Also how DXP pipelines relate to our APIs using C# code.

This document describes how to run the sample. For concept details see: [Specifying a Comparison Pipeline](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/specifying-a-comparison-pipeline)

## Running the Sample

We provide a Visual Studio solution (.sln) file for the C# sample in the *dotnet-api* directory, and a Visual Studio project (.csproj) file may be found within the sample directory *PipelineDefinition*.

# **Note - .NET support has been deprecated as of version 10.0.0 **