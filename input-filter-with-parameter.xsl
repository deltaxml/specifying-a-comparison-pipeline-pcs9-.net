<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="2.0">
  
  <xsl:param name="an-input-param" select="'default'"/>
  
  <!-- identity transform -->
  <xsl:template match="node() | @*" mode="#all">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="/">
    <xsl:message>Running input-filter-with-parameter.xsl. Parameter value is '<xsl:value-of select="$an-input-param"/>'</xsl:message>
    <xsl:apply-templates/>
  </xsl:template>
    
</xsl:stylesheet>
